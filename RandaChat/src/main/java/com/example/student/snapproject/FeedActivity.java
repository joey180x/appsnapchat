package com.example.student.snapproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class FeedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snap);

        FeedFragment login = new FeedFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.loginContainer, login).commit();
    }
}

