package com.example.student.snapproject;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainMenuFragment extends Fragment {


    public MainMenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_menu, container, false);

        String[] menuItems = {"Feed",
                                 "Friends",
                                    "Activity",
                                        "Profile"
        };
        Button button = (Button) view.findViewById(R.id.LogOutButton);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Backendless.UserService.logout(new AsyncCallback<Void>() {
                    @Override
                    public void handleResponse(Void response) {
                        Toast.makeText(getActivity(), "You logged out!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Toast.makeText(getActivity(), "Failed to log out :(", Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });


      ListView listView = (ListView) view.findViewById(R.id.mainMenu);

        ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_expandable_list_item_1,
                menuItems

             );
        listView.setAdapter(listViewAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    Intent intent = new Intent(getActivity(), FeedActivity.class);
                    startActivity(intent);
                    Toast.makeText(getActivity(), "You clicked the first item", Toast.LENGTH_SHORT).show();
                } else if (position == 1) {
                    Intent intent = new Intent(getActivity(), FriendListActivity.class);
                    startActivity(intent);
                    Toast.makeText(getActivity(), "You clicked the second item", Toast.LENGTH_SHORT).show();
                } else if (position == 2) {
                    Intent intent = new Intent(getActivity(), ActivityActivity.class);
                    startActivity(intent);
                    Toast.makeText(getActivity(), "You clicked the third item", Toast.LENGTH_SHORT).show();
                } else if (position == 3) {
                    Intent intent = new Intent(getActivity(), ProfileActivity.class);
                    startActivity(intent);
                    Toast.makeText(getActivity(), "You clicked the fourth item", Toast.LENGTH_SHORT).show();
                }


            }
        });
      return view;
    }

}