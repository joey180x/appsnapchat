package com.example.student.snapproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {

    public static final String APP_ID = "E8BAF73D-7C05-BA2D-FFFF-CD69693ACA00";
    public static final String SECRET_KEY = "E374C390-C426-BD33-FFAB-4FC720E41F00";
    public static final String VERSION = "v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       //MainMenuFragment MainMenuFragment = new MainMenuFragment();
        //getSupportFragmentManager().beginTransaction().add(R.id.container, MainMenuFragment).commit();

        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        if(Backendless.UserService.loggedInUser() == "") {
            StartFragment StartFragment = new StartFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, StartFragment).commit();
        }else {
            MainMenuFragment MainFeed = new MainMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, MainFeed).commit();
        }
    }
}

